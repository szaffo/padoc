<?php 

	session_start();

	if (! isset($_POST['email']) || !isset($_POST['passw']) || !file_exists('../database/databaseConfig.php')) {
		$_SESSION['message'] = "Belépés sikertelen.";
		header('Location: ../login.php');
		exit;
	}

	if (isset($_SESSION['id'])) {
		header('Location: index.php');
		exit;
	}


	$formemail = $_POST['email'];
	$formpassw = $_POST['passw'];

	include_once '../database/databaseConfig.php';


	$mysqli = new mysqli($dbhost, $dbuser, $dbpass, $dbtable);

	$mysqli->set_charset('utf8');

	$sql = "SELECT id FROM pas_users WHERE email = '$formemail'";

	$result = mysqli_fetch_array($mysqli->query($sql))[0];

	if ($result == null) {
		$_SESSION['message'] = 'Nincs ilyen felhasználó.';
		header('Location: ../login.php');
		exit;
	}

	$id = $result;

	$sql = "SELECT * FROM pas_users WHERE password = '$formpassw'";

	$result = mysqli_fetch_array($mysqli->query($sql));

	if ($result == null) {
		$_SESSION['message'] = 'A jelszó hibás.';
		header('Location: ../login.php');
		exit;
	}

	foreach ($result as $key => $value) {
		if (is_numeric($key)) {
			continue;
		}

		$_SESSION["$key"] = $value;
	}

	unset($_SESSION['message']);

	header('Location: index.php');
	exit;
 ?>