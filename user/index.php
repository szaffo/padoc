<?php 	

	session_start();
	if (!isset($_SESSION['id'])) {
		header('Location: ../login.php');
		exit;
	}

	if ( ! file_exists('../database/databaseConfig.php')) {
		$errormessage = 'Database not avaible.';
	} else {
		include_once('../database/databaseConfig.php');
	}

	if (!isset($errormessage)) {

		$mysqli = new mysqli($dbhost, $dbuser, $dbpass, $dbtable);

		$mysqli->set_charset('utf8');

		$sql = "SELECT pas_posts.id, pas_posts.post_name, pas_categories.name FROM pas_posts INNER JOIN pas_categories
				ON pas_categories.id = pas_posts.category_id ORDER BY category_id";

		$result = $mysqli->query($sql);

		$mysqli->close();

		$result = mysqli_fetch_all($result);

		$count = count($result) + 1;

		$sum = $count - 1;

	}

 ?>

<!DOCTYPE html>
<html lang="en">
	
	<head>
		<?php 

			include_once('../htmlsections/metaColor.html');
			include_once('../htmlsections/metatags.html');
			include_once('../htmlsections/googlefonts.html');
			include_once('../htmlsections/googleanalitics.html');
			include_once('../htmlsections/..mainCss.html');
			include_once('../htmlsections/..userCss.html');

		 ?>
		 <title>Posztok</title>
	</head>
	
	<body>

		<header>
			<?php include_once('../htmlsections/header.html'); ?>
		</header>

		<div class="table">
			<div class="aligator">
				
				<div class="left-side">
					<?php 

						echo "<div class='postline'><a target='_blank' href='../editor/?id=$count'><span class='post-name'>Új poszt</span>
							<span class='post-category'> | Összesen: $sum</span></a></div>";

						foreach ($result as $array) {
							$postid = $array[0];
							$postname = $array[1];
							$postcategory = $array[2];

							echo "<div class='postline'><a target='_blank' href='../editor/?id=$postid'><span class='post-name'>$postname</span>
							<span class='post-category'> @$postcategory</span></a></div>";
						}

					 ?>
				</div>
				<div class="right-side">

					<ul class="right-side-ul">
						<li><a class="menu-item" href="index.php">Posztok</a></li>
						<!-- <li><a class="menu-item" href="profil.php">Profil</a></li> -->
						<li><a class="menu-item" href="logout.php">Kijelentkezés</a></li>
					</ul>

				</div>

			</div>
		</div>

		<footer>
			<?php include_once '../htmlsections/footer.html'; ?>
		</footer>
		
	</body>

</html>