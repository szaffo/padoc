<?php 

	session_start();

	if (!isset($_SESSION['id'])) {
		header('Location: ../user');
	}

	if ( ! file_exists('../database/databaseConfig.php')) {
		$errormessage = 'Database not avaible.';
	} else {
		include_once('../database/databaseConfig.php');
	}

	if (!isset($errormessage)) {

		$mysqli = new mysqli($dbhost, $dbuser, $dbpass, $dbtable);

		$mysqli->set_charset('utf8');

		$sql = "SELECT COUNT(*) FROM pas_posts";

		$recordnum = $mysqli->query($sql);

		$recordnum = mysqli_fetch_array($recordnum)[0];

		$post_name = $_POST['name'];
		$post_text = $_POST['text'];
		$post_text = preg_replace("%'%", '"', $post_text);
		$category_id = $_POST['category'];
		$id = $_POST['id'];
		$author_id = $_SESSION['id'];
		$status = (isset($_POST['public'])) ? 'nocheck' : 'draft';

		if ($id > $recordnum) {
			$sql = "INSERT INTO pas_posts (author_id, status, category_id, post_text, post_name) 
					VALUES ($author_id, '$status', '$category_id', '$post_text', '$post_name')";
		} else {
			$sql = "UPDATE pas_posts SET status = '$status', category_id = $category_id, 
					post_text = '$post_text', post_name = '$post_name' WHERE id = $id";
		}

		//old but gold debug
		// echo '<pre>';
		// var_dump($sql);
		// var_dump($mysqli->query($sql));
		// echo '</pre>';

		$mysqli->query($sql);

		if ($status != 'draft') {

			$sql = "SELECT email FROM pas_users WHERE permission = 'lector'";

			$lectors = mysqli_fetch_all($mysqli->query($sql));

			include_once "classes/postmark.php";

			$postmark = new Postmark("76931b34-6fc7-4dce-9bbd-bdc06474d1ed","Pascal doksi<noreply@tob.hu>");

			foreach ($lectors as $array) {
				$email = $array[0];
				$postmark 	->to($email)
							->subject('Felülvizsgálandó lap')
							->plain_message('Egy felhasználó új posztot jelentetett meg, ami még nincs felülvizsgálva.')
							->send();
			}
		}


	}


	header('Location: ../editor/?id='.$id);

 ?>