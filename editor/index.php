<?php 

	session_start();

	if (!isset($_SESSION['id'])) {
		header('Location: ../user');
	}



	if ( ! file_exists('../database/databaseConfig.php')) {
		$errormessage = 'Database not avaible.';
	} else {
		include_once('../database/databaseConfig.php');
	}

	if (!isset($errormessage)) {

		$mysqli = new mysqli($dbhost, $dbuser, $dbpass, $dbtable);

		$mysqli->set_charset('utf8');

		$sql = "SELECT COUNT(*) FROM pas_posts";

		$recordnum = $mysqli->query($sql);

		$recordnum = mysqli_fetch_array($recordnum)[0];

		if (!isset($_GET['id'])) {
			$id = $recordnum + 1;
		} elseif ($_GET['id'] > $recordnum) {
			$id = $recordnum + 1;
		} else {
			$id = $_GET['id'];
		}

		if ($id <= $recordnum) {
			$sql = "SELECT post_text, post_name, category_id, status FROM pas_posts WHERE id = $id";
			$post = mysqli_fetch_all($mysqli->query($sql))[0];

			$text = $post[0];
			$name = $post[1];
			$category_id = $post[2];
			$status = $post[3];
		}

		$sql = "SELECT id, name FROM pas_categories";

		$categories = mysqli_fetch_all($mysqli->query($sql));

	}

 ?>

 <!DOCTYPE html>
	<html lang="en">
	
		<head>

			<!-- Chrome, Firefox OS and Opera -->
			<meta name="theme-color" content=" black">
			<!-- Windows Phone -->
			<meta name="msapplication-navbutton-color" content="black">
			<!-- iOS Safari -->
			<meta name="apple-mobile-web-app-status-bar-style" content="black">

			<link href="https://fonts.googleapis.com/css?family=Fira+Sans:200,300,500,700" rel="stylesheet">
			<link rel="stylesheet" href="../css/main.min.css">
			<link rel="stylesheet" href="../css/editor.min.css">
			<link rel="stylesheet" href="../css/code.min.css">
		
			<meta charset="utf-8">
			<meta name="author" content="Szabó Martin @tob.hu">
			<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
			<meta name="description" content="Magyar nyelvű pascal dokumentáció középiskolák számára.">
			<meta name="keywords" content=""/>
			<title>Editor</title>
			<script src="../js/syntaxer.js"></script>
		</head>
		
		<body>

			<header>
				<div class="header">
					<div class="aligator">
						<div class="logo-box">
							<h1><a class="logo" href="http:\\localhost/padoc">&#60;/Pascal&#62;</a></h1>
						</div>
						<div class="search-box">
							<input class="search-bar" type="text" placeholder="Keresés">
						</div>
					</div>
				</div>
			</header>

			<main>
				<div class="area">
					<div class="editor">
						<form action="saver.php" method="POST">
							<div class="fx-box">
								<select name="category">
									<?php 

										foreach ($categories as $array) {
											$aid = $array[0];
											$aname = $array[1];
											if ($aid == $category_id) {
												echo "<option value='$aid' selected>$aname</option>";
											} else {
												echo "<option value='$aid'>$aname</option>";
											}
										}

									 ?>
								</select>
								<input type="hidden" name="id" 
									<?php 
										echo "value='$id'";
									 ?>
								>
								<input type="text" name="name" placeholder="Posztnév"
									<?php 
										if (isset($name)) {
											echo "value='$name'";
										}
									 ?>
								>
								<input type="checkbox" name="public" id="tickbox"
									<?php 
										if (isset($status)) {
											if ($status != 'draft') {
												echo 'checked';
											}
										}
									 ?>
								>
								<label for="tickbox">Publikus</label>
							</div>
							<textarea name="text" id="text"><?php 
										if (isset($text)) {
											echo $text;	
										} 
									?></textarea>
							<button type="submit">Mentés</button>
						</form>
					</div>
					<div class="window">
						<div class="table">
							<div class="aligator">
								<div class="left-side" id="here">

									<?php 
										if (isset($text)) {
											echo $text;	
										} 
									?>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>	

			<script>
				(function() {
				   sh();
				   var textarea = document.getElementById("text");

				   	textarea.onchange = function(){
				   		var text = this.value;
				   		var wind = document.getElementById('here');
				   		wind.innerHTML = text;
				   		sh();
				   	}

				})();
			</script>	

		</body>
	</html>