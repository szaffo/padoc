<!DOCTYPE html>
<html lang="en">
	
	<head>
		<?php 

			include_once('htmlsections/metaColor.html');
			include_once('htmlsections/metatags.html');
			include_once('htmlsections/googlefonts.html');
			include_once('htmlsections/googleanalitics.html');
			include_once('htmlsections/mainCss.html');

		 ?>
		 <title>Főoldal</title>
	</head>
	
	<body>

		<header>
			<?php include_once('htmlsections/header.html'); ?>
		</header>

		<div class="table">
			<div class="aligator fd-column">
				
				<p class="low-black-text">Kedves látogató!</p>
				<p class="low-black-text">	Ez egy nem hivatalos pascal nyelvhez írt dokumentáció melyet 
					korábbi tanulók készítettek. Az oldal segítséget kínál a pasacal 
					programozási nyelv és a programozás alapjainak középiskolai
					szintű elsajátításához. Az oldalon olyan tartalmak találhatóak, 
					melyek a kezdőlépésektől, egészen az érettségire való felkészülésben
					hasznosak lehetnek.
				</p>
				<p class="low-black-text">	A posztokat középiskolai tanárok lektorálják, felülvizsgálják. Az olyan
					cikkek elején, melyek még nem lettek átnézve szakértő által, erre utaló 
					felhívás található.
				</p>
				<span><a href="categories.php"><button class="intro-button">Kategóriák</button></a></span>

			</div>
		</div>

		<footer>
			<?php include_once 'htmlsections/footer.html'; ?>
		</footer>
		
	</body>

</html>