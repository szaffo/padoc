function sh () {
	var codeboxes = document.getElementsByClassName('codebox');

	for (var i = codeboxes.length - 1; i >= 0; i--) {
		let codebox = codeboxes[i];
		let text = codebox.innerHTML;
		text = text.replace(/"/g, "'");
		text = text.replace(/('|")([\w|:|\s|\.|\/|;|\(|\)]+)('|")/g, '<span class="apostrophe">$1</span><span class="str">$2</span><span class="apostrophe">$3</span>');
		text = text.replace(/(program)(\b|\s)(\w+)(;)/gi, '$1$2<span class="prog">$3</span>$4');
		text = text.replace(/\b(as|string|longint|absolute|abstract|all|and|and_then|array|asm|attribute|begin|bindable|case|const|constructor|destructor|div|do|do|else|end|except|export|exports|external|far|file|finalization|finally|for|forward|goto|if|implementation|import|in|inherited|initialization|interface|interrupt|is|label|library|mod|module|name|near|nil|not|object|of|only|operator|or|or_else|otherwise|packed|pow|private|program|property|protected|public|published|qualified|record|repeat|resident|restricted|segment|set|shl|shr|then|to|try|type|unit|until|uses|value|var|view|virtual|while|with|xor)\b/g, '<span class="keyword">$1</span>');
		text = text.replace(/(\w+)(\(|\[\{)/g, '<span class="func">$1</span>$2');
		text = text.replace(/([\s\[\(])([0-9]+)([\s\]\)])/g, '$1<span class="number">$2</span>$3');
		text = text.replace(/(\/{2}.*)/g, '<span class="com">$1</span>');
		text = text.replace(/(\{[\s\S]*\})/g, '<span class="com">$1</span>');
		
		codebox.innerHTML = text;

	}

	console.log("Syntax highlighting done.");
}