<?php 
	
	if ( ! file_exists('database/databaseConfig.php')) {
		$errormessage = 'Database not avaible.';
	} else {
		include_once('database/databaseConfig.php');
	}

	if (!isset($_GET['cat'])) {
		header('Location: categories.php');
	} elseif (! is_numeric($_GET['cat'])) {
		header('Location: categories.php');
	}

	if (!isset($errormessage)) {

		$cat = $_GET['cat'];

		$mysqli = new mysqli($dbhost, $dbuser, $dbpass, $dbtable);

		$mysqli->set_charset('utf8');

		//check if the given number invalid
		$sql = "SELECT COUNT(id) FROM pas_categories";

		$recordnum = mysqli_fetch_array($mysqli->query($sql))[0];
		
		if ($recordnum < $cat) {
			header('Location: categories.php');
			exit;
		}

		//post data
		$sql = "SELECT id, post_name FROM pas_posts WHERE (category_id = $cat) and (status != 'draft')";

		$posts = $mysqli->query($sql);

		//category data
		$sql = "SELECT id, name FROM pas_categories WHERE (public = true) and (id = $cat)";

		$category = $mysqli->query($sql);

		$mysqli->close();

		$posts = mysqli_fetch_all($posts);
		
		$category = mysqli_fetch_all($category)[0];

		$catid = $category[0];

		$catname = $category[1];

		unset($category);

	}

 ?>


<!DOCTYPE html>
<html lang="en">
	
	<head>
		<?php 

			include_once('htmlsections/metaColor.html');
			include_once('htmlsections/metatags.html');
			include_once('htmlsections/googlefonts.html');
			include_once('htmlsections/googleanalitics.html');
			include_once('htmlsections/mainCss.html');

		 ?>
		 <title><?php echo $catname; ?></title>
	</head>
	
	<body>

		<header>
			<?php include_once('htmlsections/header.html'); ?>
		</header>

		<div class="table">
			<div class="aligator fd-column">
				
				<?php 
						if (isset($errormessage)) {
							echo $errormessage;
							exit;
						}

						echo "<h1>$catname</h1>";
				 ?>
				<a class="cats" href="categories.php">&#60;Kategóriák</a>
				<ul>
					<?php 

						foreach ($posts as $array) {
							$id = $array[0];
							$name = $array[1];
							echo "<li><a class='menu-item' href='showpost.php?id=$id'>$name</a></li>";
						}

					?>
				</ul>

			</div>
		</div>

		<footer>
			<?php include_once 'htmlsections/footer.html'; ?>
		</footer>
		
	</body>

</html>