<?php 

	session_start();

	if (isset($_SESSION["id"])) {
		header('Location: user/index.php');
	}

 ?>

<!DOCTYPE html>
<html lang="en">
	
	<head>
		<?php 

			include_once('htmlsections/metaColor.html');
			include_once('htmlsections/metatags.html');
			include_once('htmlsections/googlefonts.html');
			include_once('htmlsections/googleanalitics.html');
			include_once('htmlsections/mainCss.html');

		 ?>
		 <title>Belépés</title>
	</head>
	
	<body>

		<header>
			<?php include_once('htmlsections/header.html'); ?>
		</header>

		<div class="table">
			<div class="aligator fd-column">
				<h1>Belépés</h1>
				<?php if (isset($_SESSION['message'])) {
					$message = $_SESSION['message'];
					echo "<p class='warning'>$message</p>";
				} ?>
				<form class="" action="user/gate.php" method="POST">
					<input type="email" name="email" placeholder="Email" required>
					<input type="password" name="passw" placeholder="Jelszó" required>
					<button type="submit">Belépés</button>
				</form>

			</div>
		</div>

		<footer>
			<?php include_once 'htmlsections/footer.html'; ?>
		</footer>
		
	</body>

</html>