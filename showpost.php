<?php 
	
	if ( ! file_exists('database/databaseConfig.php')) {
		$errormessage = 'Database not avaible.';
	} else {
		include_once('database/databaseConfig.php');
	}

	if (!isset($errormessage)) {

		$id = $_GET['id'];

		$mysqli = new mysqli($dbhost, $dbuser, $dbpass, $dbtable);

		$mysqli->set_charset('utf8');

		//check if the given number invalid
		$sql = "SELECT COUNT(id) FROM pas_posts";

		$recordnum = mysqli_fetch_array($mysqli->query($sql))[0];

		if ($recordnum < $id) {
			header('Location: categories.php');
			exit;
		}

		//post data
		$sql = "SELECT post_name, post_text, author_id, modified, category_id, status
				FROM pas_posts WHERE (id = $id)";

		$post = $mysqli->query($sql);

		$post = mysqli_fetch_all($post)[0];

		$postname = $post[0];
		$posttext = $post[1];
		$authorid = $post[2];
		$modified = $post[3];
		$categoryid = $post[4];
		$status = $post[5];

		$sql = "SELECT name, email FROM pas_users WHERE id = $authorid";

		$author = mysqli_fetch_all($mysqli->query($sql))[0]; //one line style :D

		$authorname = $author[0];
		$authoremail = $author[1];

		$sql = "SELECT post_name, id FROM pas_posts WHERE (status != 'draft') and (category_id = $categoryid)";
		
		$sideposts = mysqli_fetch_all($mysqli->query($sql)); //one line style :D  //copy paste style :P

		$sql = "SELECT name FROM pas_categories WHERE id = $categoryid";

		$categoryname = mysqli_fetch_all($mysqli->query($sql))[0][0];

		$mysqli->close();

	}

 ?>


<!DOCTYPE html>
<html lang="hu">
	
	<head>
		<?php 

			include_once('htmlsections/metaColor.html');
			include_once('htmlsections/metatags.html');
			include_once('htmlsections/googlefonts.html');
			include_once('htmlsections/googleanalitics.html');
			include_once('htmlsections/mainCss.html');

		 ?>
		 <title><?php echo $postname; ?></title>
		 <link rel="stylesheet" href="css/code.min.css">
		 <script src="js/syntaxer.js"></script>
	</head>
	
	<body>

		<header>
			<?php include_once('htmlsections/header.html'); ?>
		</header>

		<div class="table">
			<div class="aligator">
				
				<div class="left-side">
					<?php 

						if (isset($errormessage)) {
							echo $errormessage;
							exit;
						}

						if ($status == 'draft') {
							echo "<p class='info'>Ez egy piszkozat, nem jelenik meg az oldalon.</p>";
						} elseif ($status == 'nocheck') {
							echo "<p class='warning'>Ezt a lapot még nem nézte át senki sem. Előfordulhatnak benne hibák.</p>";
						}

						echo $posttext;

						$modified_dateonly = substr($modified, 	0, strpos($modified, ' '));
						echo "<p class='date'>$modified_dateonly</p>";

					 ?>
				</div>
				<div class="right-side">

					<?php echo "<h1 class='side-cat'><a class='side-cat' href='posts.php?cat=$categoryid'>$categoryname</a></h1>"; ?>
					<ul class="right-side-ul">

						<?php 

							foreach ($sideposts as $array) {
								$sideid = $array[1];
								$sidename = $array[0];
								echo "<li><a class='menu-item' href='showpost.php?id=$sideid'>$sidename</a></li>";
							}

						 ?>

					 </ul>

				</div>

			</div>
		</div>

		<footer>
			<?php include_once 'htmlsections/footer.html'; ?>
		</footer>

		<script>
			sh();
		</script>
		
	</body>

</html>