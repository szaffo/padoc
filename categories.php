<?php 

	if ( ! file_exists('database/databaseConfig.php')) {
		$errormessage = 'Database not avaible.';
	} else {
		include_once('database/databaseConfig.php');
	}

	if (!isset($errormessage)) {

		$mysqli = new mysqli($dbhost, $dbuser, $dbpass, $dbtable);

		$mysqli->set_charset('utf8');

		$sql = "SELECT id, name FROM pas_categories WHERE public = true";

		$categories = $mysqli->query($sql);

		$mysqli->close();

		$categories = mysqli_fetch_all($categories);

	}

 ?>

<!DOCTYPE html>
<html lang="en">
	
	<head>
		<?php 

			include_once('htmlsections/metaColor.html');
			include_once('htmlsections/metatags.html');
			include_once('htmlsections/googlefonts.html');
			include_once('htmlsections/googleanalitics.html');
			include_once('htmlsections/mainCss.html');

		 ?>
		 <title>Kategóriák</title>
	</head>
	
	<body>

		<header>
			<?php include_once('htmlsections/header.html'); ?>
		</header>

		<div class="table">
			<div class="aligator fd-column">
				
				<h1>Kategóriák</h1>

				<ul>
					<?php 
						if (isset($errormessage)) {
							echo $errormessage;
							exit;
						}

						foreach ($categories as $array) {
							$id = $array[0];
							$name = $array[1];
							echo "<li><a class='menu-item' href='posts.php?cat=$id'>$name</a></li>";
						}
					 ?>
				</ul>

			</div>
		</div>

		<footer>
			<?php include_once 'htmlsections/footer.html'; ?>
		</footer>
		
	</body>

</html>