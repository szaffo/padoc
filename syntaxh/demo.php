<body style="margin:0px; color: #f2f2f2; background: #333; padding: 30px; font-size: 20px;">

<pre>

<?php 

	$pattern = [

		'program <span style="color: yellowgreen;">\\1</span>;' => '%program\s*(.*)\;%',
		'<span style="color: fuchsia;"><i>\\1</i></span>\\2' => '%\b(as|string|longint|absolute|abstract|all|and|and_then|array|asm|attribute|begin|bindable|case|class|const|constructor|destructor|div|do|do|else|end|except|export|exports|external|far|file|finalization|finally|for|forward|goto|if|implementation|import|in|inherited|initialization|interface|interrupt|is|label|library|mod|module|name|near|nil|not|object|of|only|operator|or|or_else|otherwise|packed|pow|private|program|property|protected|public|published|qualified|record|repeat|resident|restricted|segment|set|shl|shr|then|to|try|type|unit|until|uses|value|var|view|virtual|while|with|xor)\b%',
		'<span style="color: deepskyblue;"><i>\\1</i></span>\\2' => '%(\w*)\s?(\(.*\))%',
		'<span style="color: lightgreen;">\\1</span>' => '%(\'.*\')%',
		'\\1<span style="color: orange;">\\2</span>' => '%(\s|\[)([0-9]+)%',
		'<span style="color: lightgray; opacity: 0.3;">\\1</span>' => '%(\{[\s|\w|.]*})%',
		'<span style="color: lightgray;  opacity: 0.3;">\\1</span>' => '%(\/\/.*)%',
	];

	include_once 'sh.php';

	$code = new SH();


	echo $code->loadFile('example.pas')->addPattern($pattern)->magic();

 ?>

</pre>

<!-- * zero or more times -->