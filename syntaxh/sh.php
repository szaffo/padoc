<?php

	class SH {
		private $text;

		private $patterns;

		public function loadFile($path) {

			$text = file_get_contents($path);

			$this->text = $text;

			return $this;

		}//loadFile()

		public function addPattern($array) {

			$this->patterns = $array;

			return $this;

		}//addPattern()

		public function magic() {

			$text = $this->text;

			foreach ($this->patterns as $replace => $pattern) {
				$text = preg_replace($pattern, $replace, $text);
			}//foreach

			return $text;

		}//magic()
	}

	$code = new SH();

	//print $code->loadFile('sh.php')->addPattern(['/(class)/' => 'purple', '/(SH)/' => 'red'])->magic();


  ?>