-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2017. Júl 30. 20:53
-- Kiszolgáló verziója: 10.1.19-MariaDB
-- PHP verzió: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `tob_hu`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `pas_posts`
--

CREATE TABLE `pas_posts` (
  `id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(10) COLLATE utf8_hungarian_ci NOT NULL DEFAULT 'draft',
  `category_id` int(11) NOT NULL,
  `post_text` varchar(5000) COLLATE utf8_hungarian_ci NOT NULL,
  `post_name` varchar(125) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `pas_posts`
--

INSERT INTO `pas_posts` (`id`, `author_id`, `modified`, `status`, `category_id`, `post_text`, `post_name`) VALUES
(1, 2, '2017-07-29 13:33:05', 'nocheck', 1, '<h1>length</h1>\r\n<div class="def-box">\r\n<p><span style="color: cornflowerblue">length(input</span> <span style="color: forestgreen">:mixed</span><span style="color: cornflowerblue">)</span> <span style="color: orange">:number</span></p>\r\n</div>\r\n<p>A megadott string hosszával tér vissza, ami maximum 255. Ha a bemenet üres string, akkor 0-val tér visssza.</p>\r\n<p>Ha a bemenet tömb, akkor az elemek számát adja meg.</p>\r\n<h2>Paraméterek</h2>\r\n<h3>input</h3>\r\n<p>String vagy array aminek megszámolja az elemeit. Típusa lehet string vagy array.</p>\r\n\r\n\r\n<h2>Visszatérés</h2>\r\n<h3>number</h3>\r\n<p>A megadott bemenet elemeinek száma. Típusa longint.</p>\r\n\r\n<h2>Példaprogramok</h2>\r\n<div class="codebox">program Pelda0;\r\n\r\n	var\r\n		szoveg: string[23];\r\n		szam: longint;\r\n\r\n	begin\r\n		write("Szoveg (max 23 karakter): ");\r\n		readln(szoveg);\r\n\r\n		for szam := 1 to length(szoveg) do\r\n			begin\r\n			 	writeln(szoveg[szam]);\r\n			 end; \r\n\r\n		//asd(asdas); 56\r\n\r\n		{ asdww safa 4\r\n		}\r\n\r\n	end.\r\n</div>', 'length'),
(2, 1, '2017-07-24 21:00:59', 'public', 2, '<h1>teszt</h1>\r\n						<div class="def-box">\r\n							<p><span style="color: cornflowerblue">length(input</span> <span style="color: forestgreen">:mixed</span><span style="color: cornflowerblue">)</span> <span style="color: orange">:number</span></p>\r\n						</div>\r\n						<p>A megadott string hosszával tér vissza, ami maximum 255. Ha a bemenet üres string, akkor 0-val tér visssza.</p>\r\n						<p>Ha a bemenet tömb, akkor az elemek számát adja meg.</p>\r\n						<h2>Paraméterek</h2>\r\n						<h3>input</h3>\r\n						<p>String vagy array aminek megszámolja az elemeit. Típusa lehet string vagy array.</p>\r\n						\r\n\r\n						<h2>Visszatérés</h2>\r\n						<h3>number</h3>\r\n						<p>A megadott bemenet elemeinek száma. Típusa longint.</p>\r\n						\r\n						<h2>Példaprogramok</h2>', 'Teszt'),
(3, 2, '2017-07-28 14:20:17', 'nocheck', 1, '<h1>Lengő </h1>\r\n						<div class="def-box">\r\n							<p><span style="color: cornflowerblue">length(input</span> <span style="color: forestgreen">:mixed</span><span style="color: cornflowerblue">)</span> <span style="color: orange">:number</span></p>\r\n						</div>\r\n						<p>A megadott string hosszával tér vissza, ami maximum 255. Ha a bemenet üres string, akkor 0-val tér visssza.</p>\r\n						<p>Ha a bemenet tömb, akkor az elemek számát adja meg.</p>\r\n						<h2>Paraméterek</h2>\r\n						<h3>input</h3>\r\n						<p>String vagy array aminek megszámolja az elemeit. Típusa lehet string vagy array.</p>\r\n						\r\n\r\n						<h2>Visszatérés</h2>\r\n						<h3>number</h3>\r\n						<p>A megadott bemenet elemeinek száma. Típusa longint.</p>\r\n						\r\n						<h2>Példaprogramok</h2>', 'Ez egy másik teszt'),
(4, 2, '2017-07-30 20:48:38', 'draft', 2, '<h1>Ez egy tesztképpen létrehozott oldal</h1>', 'tesztelés'),
(5, 2, '2017-07-28 17:59:20', 'nocheck', 2, 'semmi', 'Posztnak a neve'),
(6, 2, '2017-07-30 20:44:44', 'draft', 2, '<h1>not</h1>\r\n<h2>Lorem Ipsum</h2>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur congue odio ac odio euismod, eu viverra purus venenatis. Aenean in neque ut odio bibendum auctor in a orci. Duis eu molestie metus. Donec at fringilla ligula, pharetra rutrum orci. In ac urna non dolor imperdiet pretium. Quisque vel turpis id est dignissim finibus sed eget massa. Sed lacus dui, consequat nec convallis nec, ornare ac velit. Aenean nunc erat, condimentum in dui eu, tincidunt maximus dolor. Nulla dapibus libero in libero aliquam, nec commodo sem mattis. Nulla a aliquet urna. Morbi fermentum, mi eget facilisis laoreet, lorem leo mattis felis, quis euismod sem odio at magna. Vestibulum finibus imperdiet libero in venenatis. In porta ipsum a purus maximus commodo.</p>\r\n<h2>Lorem Ipsum</h2>\r\n<h3>Lorem</h3>\r\n<p>Cras nec lacus ut enim eleifend venenatis. Praesent luctus convallis purus vitae suscipit. Nunc pulvinar id dolor non rhoncus. Sed a finibus risus, sit amet dictum nulla. Fusce sit amet consequat urna. Donec vitae mattis sapien. Aenean mattis ornare nibh sodales finibus.</p>\r\n<h3>Ipsum</h3>\r\n<p>Aenean mattis, nibh ac tempor venenatis, lacus leo pharetra enim, a cursus ante ante ac nunc. In in augue odio. Nullam vel sapien elit. Curabitur eros odio, facilisis ut lectus quis, ultrices facilisis nunc. Nunc nec est ultricies, semper nibh eu, scelerisque nisi. Maecenas pretium pharetra faucibus. Aenean non dolor libero. Proin ut interdum nibh. Vivamus id laoreet velit. Quisque porttitor eleifend dolor vel mattis. Sed aliquet facilisis hendrerit. Proin in urna gravida, dictum mi eget, lacinia lorem. Sed tellus ipsum, elementum vel sapien et, feugiat pharetra libero. Praesent sed eleifend diam.</p>', 'Lorem ipsum');

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `pas_posts`
--
ALTER TABLE `pas_posts`
  ADD PRIMARY KEY (`id`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `pas_posts`
--
ALTER TABLE `pas_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
